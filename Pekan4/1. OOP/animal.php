<?php

class Animal
{
    public $legs, $cold_blooded, $name;

    public function __construct($name)
    {
        $this->name = $name;
        $this->legs = 4;
        $this->cold_blooded = false;
    }
}
