<?php

class Ape extends Animal
{
    function __construct($name)
    {
        $this->name = $name;
        $this->legs = 2;
        $this->cold_blooded = false;
    }

    public function yell()
    {
        echo "Auooo";
    }
}
