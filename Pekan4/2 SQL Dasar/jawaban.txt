-- 1. Buat database
create database myshop;

-- 2. Buat tabel Users, Categories, dan Items
create table users (
    id int primary key auto_increment,
    name varchar(255) not null,
    email varchar(255) not null,
    password varchar(255) not null
);

create table categories (
    id int primary key auto_increment,
    name varchar(255) not null
);

create table items (
    id int primary key auto_increment,
    category_id int not null,
    name varchar(255) not null,
    description varchar(255) not null,
    price int not null,
    stock int not null,
    foreign key (category_id) references categories(id)
);

-- 3. Insert Data
insert into users (name, email, password) values
('John Doe', 'john@doe.com', 'john123'),
('Jane Doe', 'jane@doe.com', 'jenita123');

insert into categories (name) values
('gadget'),
('cloth'),
('man'),
('woman'),
('branded');

insert into items (category_id, name, description, price, stock) values
(1, 'Sumsang b50', 'hape keren dari merek sumsang', 4000000, 100),
(2, 'Uniklooh', 'baju keren dari brand ternama', 500000, 50),
(1, 'IMHO Watch', 'jam tangan anak yang jujur banget', 2000000, 10);